#!/usr/bin/env python
# -*- coding: UTF8 -*-

# Author: Guillaume Bouvier -- guillaume.bouvier@pasteur.fr
# https://research.pasteur.fr/en/member/guillaume-bouvier/
# 2017-10-18 08:37:08 (UTC+0200)

import numpy
import PDBparser

def firstquarter(angle):
    if angle < 90:
        return angle
    elif angle <180:
        return 180-angle
    elif angle <270:
        return  angle-180
    else:
        return 360-angle

class PCA(object):
    """
    Compute the PCA on the given path coordinates
    """
    def __init__(self, coords):
        self.coords = numpy.asarray(coords)
        self.start = numpy.copy(self.coords[0][0])
        self.stop = numpy.copy(self.coords[0][-1])
        n, l, d = self.coords.shape
        self.coords = self.coords.reshape((n*l, d))
        self.coords -= self.coords.mean(axis=0)
        self.covariance = self.coords.T.dot(self.coords) / (n*l)
        self.eigvals, self.eigvecs = self.eigh()
        self.alpha, self.beta, self.gamma = self.angles()

    def eigh(self):
        """
        Get the eigenvalues and eigenvectors
        """
        eigvals, eigvecs = numpy.linalg.eigh(self.covariance)
        sorter = eigvals.argsort()[::-1]
        eigvecs = eigvecs[:, sorter]
        eigvals = eigvals[sorter]
        return eigvals, eigvecs

    def angles(self):
        """
        Returns the angles alpha, beta and gamma between the vectors of the
        starting and ending point of the path and the 3 eigenvectors.
        """
        v = self.stop - self.start
        v /= numpy.linalg.norm(v)
        alpha = numpy.arccos(v.dot(self.eigvecs[:,0]))
        alpha = numpy.rad2deg(alpha)
        beta = numpy.arccos(v.dot(self.eigvecs[:,1]))
        beta = numpy.rad2deg(beta)
        gamma = numpy.arccos(v.dot(self.eigvecs[:,2]))
        gamma = numpy.rad2deg(gamma)
        return firstquarter(alpha), firstquarter(beta), firstquarter(gamma)

    @property
    def vectors(self):
        """
        Get the 3 eigenvectors scaled with the eigenvalues and centered on the
        data
        """
        v1 = [self.start, self.start + 2*numpy.sqrt(self.eigvals[0])*self.eigvecs[:, 0]]
        v1 = numpy.asarray(v1)
        v2 = [self.start, self.start + 2*numpy.sqrt(self.eigvals[1])*self.eigvecs[:, 1]]
        v2 = numpy.asarray(v2)
        v3 = [self.start, self.start + 2*numpy.sqrt(self.eigvals[2])*self.eigvecs[:, 2]]
        v3 = numpy.asarray(v3)
        return v1, v2, v3

    def write_vectors(self):
        """
        Write the three vectors in a PDB file
        """
        v1, v2, v3 = self.vectors
        data = numpy.asarray([self.start, v1[1], self.start, v2[1], self.start, v3[1]])
        connect = [[1, 2], [3, 4], [5, 6]]
        PDBparser.write_pdb(data, connect=connect, outfilename='eigenvectors.pdb')
