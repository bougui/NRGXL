#!/usr/bin/python

import numpy
import scipy.spatial
import scipy.sparse
import scipy.ndimage
import pdb
import time
import os
import sys

class Grid:

    def __init__(self,proteinxyz,gridCellLength,threshold):
        self.gridCellLength = gridCellLength
        self.edgeLength = numpy.array([(proteinxyz[:,i]).max()-(proteinxyz[:,i]).min() for i in range(3)])
        self.edgeLength += 2 * gridCellLength+2*threshold
        self.center = numpy.array(proteinxyz.max(axis=0)+proteinxyz.min(axis=0))/2
        self.xgrid = numpy.linspace(self.center[0] - .5 * self.edgeLength[0], self.center[0] + .5 * self.edgeLength[0],
                           self.edgeLength[0] / gridCellLength)
        self.ygrid = numpy.linspace(self.center[1] - .5 * self.edgeLength[1], self.center[1] + .5 * self.edgeLength[1],
                           self.edgeLength[1] / gridCellLength)
        self.zgrid = numpy.linspace(self.center[2] - .5 * self.edgeLength[2], self.center[2] + .5 * self.edgeLength[2],
                           self.edgeLength[2] / gridCellLength)
        self.counts, self.edges = numpy.histogramdd(proteinxyz, bins=(self.xgrid, self.ygrid, self.zgrid))
        self.counts = scipy.ndimage.morphology.distance_transform_edt(self.counts<1)
        self.counts = self.counts < threshold/float(gridCellLength)


    def vmdMap(self,outfilename):
        """
        From the density grid, create a document to visualize it in VMD
        """
        self.center = tuple(numpy.array(self.center) + .5 * self.gridCellLength)
        outfile = open(outfilename, 'w')
        nx, ny, nz = self.counts.shape
        outfile.write("GRID_PARAMETER_FILE NONE\n")
        outfile.write("GRID_DATA_FILE NONE\n")
        outfile.write("MACROMOLECULE NONE\n")
        outfile.write("SPACING %s\n" % self.gridCellLength)
        outfile.write("NELEMENTS %d %d %d\n" % (nx - 1, ny - 1, nz - 1))
        outfile.write("CENTER %.2f %.2f %.2f\n" % self.center)
        for z in range(nz):
            for y in range(ny):
                for x in range(nx):
                    outfile.write("%.3f\n" % (self.counts[x, y, z]))

    def get_adjmat(self,voidRelevance):
        """
        Compute the adjacency matrix from the EM density
        """
        self.grid = numpy.array(numpy.meshgrid(self.xgrid[:-1], self.ygrid[:-1], self.zgrid[:-1], indexing='ij'))
        self.grid = self.grid.reshape(3, self.xgrid[:-1].size * self.ygrid[:-1].size * self.zgrid[:-1].size)
        self.grid = self.grid.T
        self.counts2 = self.counts.reshape(1, self.xgrid[:-1].size * self.ygrid[:-1].size * self.zgrid[:-1].size)
        self.counts2 = numpy.squeeze(self.counts2.T)
        kdtree = scipy.spatial.cKDTree(self.grid)
        index = kdtree.query_ball_point(self.grid, 2 * self.gridCellLength)
        row = []
        col = []
        data = []
        for i, index_ in enumerate(index):
            index_ = set(index_) - set([i])
            for j in index_:
                row.append(i)
                col.append(j)
                data.append((self.counts2[i] + self.counts2[j]) + voidRelevance)
        self.adjmat = scipy.sparse.csr_matrix((data, (row, col)), shape=(self.counts.size,) * 2)


def get_atom_cell(atomId,xgrid,ygrid,zgrid,coord):
    """
    Given an atomId, return its position in the grid. In this we do not compute again the grid.
    """
    atomCoord = coord[atomId]
    x=numpy.digitize(numpy.array(atomCoord[0]),bins=(xgrid))
    y=numpy.digitize(atomCoord[1],bins=(ygrid))
    z=numpy.digitize(atomCoord[2], bins=(zgrid))
    cellIndex = ((len(zgrid)-1)*(len(ygrid)-1)*(x-1)+(len(zgrid-1)-1)*(y-1)+(z-1))
    return cellIndex

def scipy_dijkstra(adjmat,startAtomCell,finishAtomCell):
    """
    Compute the shortest path between two atoms using the dijkstra implemented in scipy package.
    """

    dist_matrix, predecessors = scipy.sparse.csgraph.dijkstra(adjmat, return_predecessors=True,
                                                              indices=(startAtomCell, finishAtomCell), directed=True)
    pathAtoms = int(dist_matrix[0][finishAtomCell])
    path = []
    path.append(finishAtomCell)
    prev = finishAtomCell
    while prev != startAtomCell:
        prev = predecessors[0][prev]
        path.append(prev)
    path = numpy.flip(path, axis=0)
    return path, pathAtoms

def get_grid(gridCellLength,voidRelevance,coord,threshold,grids):
    """
    Compute the grid and its adjacency matrix for a given PDB and a given cell length
    """

    """
        Build the grid if we do not have it yet
    """
    if "grid_{0}".format(gridCellLength) not in grids.keys():
        grids["grid_{0}".format(gridCellLength)] = Grid(coord,gridCellLength,threshold)
        print('Building the grid with gridCellLength {0} and threshold {1}'.format(gridCellLength,threshold))
        first=True
    else:
        first=False
    if first:
        outputEM = 'output/density_' + str(gridCellLength) + '_' + str(threshold) + '.map'
        grids["grid_{0}".format(gridCellLength)].vmdMap(outputEM)

    """
        Build the adjacency matrix and print the grid
    """
    if first:
        grids["grid_{0}".format(gridCellLength)].get_adjmat(voidRelevance)
        print('Building the adjacent matrix with gridCellLength {0} and threshold {1}'.format(gridCellLength,threshold))
    grid = grids["grid_{0}".format(gridCellLength)].grid
    if first:
        outputGrid = 'output/grid_' + str(gridCellLength) + '_' + str(threshold) + '.pdb'
        #pdb.write_pdb(grid, occupancy=tmp, outfilename=outputGrid)

    return grid,grids



def shortest_path(gridCellLength,startResId,finishResId
                  ,startChainId,finishChainId,atom_types, aa_list, chain_ids, resids, coord, grids,grid, threshold):

    start_time = time.time()
    """
        Id's of starting and finishing atom of the given residue
    """
    startAtomId=pdb.ResChainID_to_NZatomId (startResId,startChainId,chain_ids,atom_types,resids,aa_list)
    finishAtomId=pdb.ResChainID_to_NZatomId (finishResId,finishChainId,chain_ids,atom_types,resids,aa_list)
    if startAtomId==-1 or finishAtomId==-1:
        print ("-----------------------------------------------")
        sys.exit("Some of the residues that you gave aren't LYS\n-----------------------------------------------")

    """
        Compute the shortest path between atoms
    """
    xgrid = grids["grid_{0}".format(gridCellLength)].xgrid
    ygrid = grids["grid_{0}".format(gridCellLength)].ygrid
    zgrid = grids["grid_{0}".format(gridCellLength)].zgrid
    adjmat = grids["grid_{0}".format(gridCellLength)].adjmat
    startAtomCell = get_atom_cell(startAtomId,xgrid,ygrid,zgrid,coord)
    finishAtomCell = get_atom_cell(finishAtomId,xgrid,ygrid,zgrid,coord)
    path, pathAtoms = scipy_dijkstra(adjmat,startAtomCell,finishAtomCell)
    outputPath = 'output/path_' + str(startResId) + startChainId + '_' + str(finishResId) \
                 + finishChainId + '_' + str(gridCellLength) + '_' + str(threshold) + '.pdb'
    pdb.write_pdb(grid[path],outfilename=outputPath)
    pathLength = sum(numpy.linalg.norm(numpy.diff(grid[path],axis=0),axis=1))

    """
        Printing
    
    print('\nThe length of the path between the residue %d and the residue %d is %f ' % (startResId, finishResId, pathLength))
    print('During this path we pass through %d cells and %d atoms\n ' % (len(path), pathAtoms))
    print('Atom starting coords: ', coord[startAtomId],'\nAtom finish coords: ', coord[finishAtomId], '\n')
    print('Cell starting coords: ', grid[startAtomCell],'\nCell finish coords: ', grid[finishAtomCell], '\n')
    print("The execution took --- %s seconds ---" % (time.time() - start_time))
    """

    finish_time = time.time() - start_time

    return pathLength,path,pathAtoms,finish_time


if __name__ == '__main__':

    inputname = sys.argv[1]
    imputindex = sys.argv[2]
    threshold = int(sys.argv[3])
    voidRelevance = 0.00000000001
    gridCellLength = 4
    grids = {}
    imputfile = open(imputindex,"r")
    outputfile = open("lysineDistancesParallel_"+str(threshold)+'_' + str(gridCellLength) +".csv", "a")

    outputfile.write('StartRes;FinishRes;StartChain;FinishChain;PathDistance\n')

    if not os.path.exists('output'):
        os.makedirs('output')

    atom_types, aa_list, chain_ids, resids, coord, is_ca_trace = pdb.read_pdb(inputname)

    count=0
    for line in imputfile:
        parameters = line.split()
        startResId = int(parameters[0])
        finishResId = int(parameters[1])
        startChainId = str(parameters[2])
        finishChainId = str(parameters[3])
        grid, grids = get_grid(gridCellLength,voidRelevance,coord,threshold,grids)
        pathLength,path,pathAtoms,finish_time = shortest_path(
            gridCellLength,startResId,finishResId,startChainId,finishChainId,
            atom_types, aa_list, chain_ids, resids, coord, grids,grid,threshold)
        outputfile.write('%d;%d;%s;%s;%f\n' %
                     (startResId, finishResId, startChainId, finishChainId, pathLength))
        print(count)
        count+=1

    outputfile.close()
    imputfile.close()