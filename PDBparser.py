#!/usr/bin/env python
# -*- coding: UTF8 -*-

import numpy

def read_pdb(pdbfilename):
    """
    Read the given PDB filename and returns the atom_types, aa_list and a
    numpy array with the coordinates
    """
    splitted_lines = []
    with open(pdbfilename) as pdbfile:
        for line in pdbfile:
            if line[:4] == 'ATOM':
                splitted_line = [line[:6], line[6:11], line[12:16], line[17:20], line[21], line[22:26], line[30:38], line[38:46], line[46:54]]
                splitted_lines.append(splitted_line)
    splitted_lines = numpy.asarray(splitted_lines)
    atom_types = [e.strip() for e in splitted_lines[:, 2]]
    aa_list = [e.strip() for e in splitted_lines[:, 3]]
    chain_ids = [e.strip() for e in splitted_lines[:, 4]]
    resids = [int(e) for e in splitted_lines[:, 5]]
    # Check if the given pdbfile is a CA trace only
    is_ca_trace = len(set(resids)) == len(resids)
    return atom_types, aa_list, chain_ids, resids, numpy.float_(splitted_lines[:, 6:9]), is_ca_trace

def write_pdb(data, occupancy=None, outfilename="points.pdb", connect=None,
              sequence=None, atoms=None, chain_ids=None,
              resids=None, resnames=None, multimodel=False, write_conect=True,
              hetatm=False):
    """
    write data (coordinates of points (n*3)) in a pdb file
    • sequence: if not None use the sequence (list of 1 letter code) to write to
    the PDB
    • atoms: list of atom types
    • chain_ids: list of chain ids
    • resids: list of residue ids
    • write_conect: If True write the CONECT fields
    • multimodel: Write a multimodel PDB. Then data must be a list of
    coordinates
    """
    one_to_three = {'R': 'ARG', 'H': 'HIS', 'K': 'LYS', 'D': 'ASP', 'E': 'GLU',
                    'S': 'SER', 'T': 'THR', 'N': 'ASN', 'Q': 'GLN', 'C':'CYS',
                    'G': 'GLY', 'P': 'PRO', 'A': 'ALA', 'V': 'VAL', 'I': 'ILE',
                    'L': 'LEU', 'M': 'MET', 'F': 'PHE', 'Y': 'TYR', 'W': 'TRP',
                    'X': 'UNK'}
    aa_list = None
    if sequence is not None:
        aa_list = [one_to_three[e] if len(e) == 1 else e for e in sequence]
    elif resnames is not None:
        aa_list = resnames
    if not multimodel:
        data = [data, ]

    def default_values(value):
        outlist = []
        for data_ in data:
            outlist.append([value, ]*data_.shape[0])
        return outlist

    if aa_list is None:
        aa_list = default_values('BSX')
    else:
        if not multimodel:
            aa_list = [aa_list, ]
    if atoms is None:
        atoms = default_values('CA')
    else:
        if not multimodel:
            atoms = [atoms, ]
    if chain_ids is None:
        chain_ids = default_values('Z')
    else:
        if not multimodel:
            chain_ids = [chain_ids, ]
    if resids is None:
        resids = default_values(999)
    else:
        if not multimodel:
            resids = [resids, ]
    if occupancy is None:
        occupancy = default_values(1.)
    else:
        if not multimodel:
            occupancy = [occupancy, ]
    with open(outfilename, 'w') as outfile:
        for model_id, data_ in enumerate(data):
            if multimodel:
                outfile.write("MODEL %d\n"%(model_id+1))
            for i, e in enumerate(data_):
                x, y, z = e
                x, y, z = "%.3f"%x, "%.3f"%y,"%.3f"%z
                aa = aa_list[model_id][i]
                atom = atoms[model_id][i]
                chain_id = chain_ids[model_id][i]
                resid = resids[model_id][i]
                o = "%.2f"%occupancy[model_id][i]
                if not hetatm:
                    outfile.write("%-6s%5s %3s  %3s %s%4s    %8s%8s%8s%6s 20.00      %s    %s\n"%("ATOM", i+1, atom, aa, chain_id, resid, x, y, z, o, chain_id, atom[0]))
                else:
                    outfile.write("%-6s%5s %3s  %3s %s%4s    %8s%8s%8s%6s 20.00      %s    %s\n"%("HETATM", i+1, atom, aa, chain_id, resid, x, y, z, o, chain_id, atom[0]))
            if multimodel:
                outfile.write("ENDMDL\n")
            if write_conect and model_id + 1 == len(data):
                if connect is None:
                    connect = []
                    n = len(data_)
                    for i in range(n-1):
                        connect.append([i+1, i+2])
                if connect is not None:
                    for connect_ids in connect:
                        if len(connect_ids) > 1:
                            outfile.write("CONECT")
                            for a in connect_ids:
                                outfile.write("%5d"%a)
                            outfile.write("\n")
        outfile.write("TER\n")
        outfile.write("END")

def ResChainID_to_CAatomId (resID,chainID,chain_ids,atom_types,resids):
    """
    Given a chain of the molecule and a residue of it, it return the AtomId of its CA
    """
    aux=[]
    for i, resids_ in enumerate(resids):
        if resids_ == resID:
            aux.append(i)
    aux2=[]
    for i, aux_ in enumerate(aux):
        if atom_types[aux_] == 'CA':
            aux2.append(aux_)
    for i, aux2_ in enumerate(aux2):
        if chain_ids[aux2_] == chainID:
            return aux2_

def ResChainID_to_NZatomId (resID,chainID,chain_ids,atom_types,resids,aa_list):
    """
    Given a chain of the molecule and a Lysine of it, it return the AtomId of its NZ
    """
    aux=[]
    for i, resids_ in enumerate(resids):
        if resids_ == resID:
            aux.append(i)
    aux2=[]
    for i,aux_ in enumerate(aux):
        if aa_list[aux_] == 'LYS':
            aux2.append(aux_)
    if len(aux2)==0:
        #In case that the residue is not a LYS
        return -1
    aux=[]
    for i, aux2_ in enumerate(aux2):
        if atom_types[aux2_] == 'NZ':
            aux.append(aux2_)
    for i, aux_ in enumerate(aux):
        if chain_ids[aux_] == chainID:
            return aux_
