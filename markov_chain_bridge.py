#!/usr/bin/env python
# -*- coding: UTF8 -*-

# Author: Guillaume Bouvier -- guillaume.bouvier@pasteur.fr
# https://research.pasteur.fr/en/member/guillaume-bouvier/
# 2016-09-05 16:01:43 (UTC+0200)

import numpy
import sys
sys.path.append('/home/bougui/source/progress_reporting/')
import progress_reporting
import scipy.sparse

def identity_sparse(n):
    """
    Return the identity matrix as a csr sparse matrix
    """
    ind = numpy.arange(n)
    diag = numpy.ones(n)
    return scipy.sparse.csr_matrix((diag, (ind, ind)), shape=(n,n))

class MCB(object):
    """
    Compute a discrete-time Markov Chain bridge from the transition matrix.
    Sample trajectories from the Markov Chain such that:

    1) The size/length of the trajectory is N.
    2) Trajectory at step 0 is in state i0.
    3) Trajectory at step N is in state iN.
    """
    def __init__(self, transition_matrix, nstep, x_stop):
        self.transition_matrix = transition_matrix
        self.x_stop = x_stop
        self.power_mats = [identity_sparse(transition_matrix.shape[0])[:, x_stop], ]
        self.nstep = nstep
        #progress = progress_reporting.Progress(n_step=nstep-1,
        #                                       label='Computing matrix power')
        for i in range(1, nstep):
            self.power_mats.append(transition_matrix.dot(self.power_mats[i-1]))
            #progress.count(report='^%d'%i)

    def mc_bridge(self, x_start):
        """
        Compute the discrete-time Markov Chain bridge from x_start to x_stop
        with nstep steps
        """
        n_states = self.transition_matrix.shape[0]
        # Trajectory
        trajectory = []
        trajectory.append(x_start)
        directVisitedNeighbours = set()
        for i in range(1, self.nstep):
            pij = self.transition_matrix[trajectory[i-1], :]
            pij = numpy.squeeze(numpy.asarray(pij.todense()))
            pjiN = self.power_mats[self.nstep-i-1]
            pjiN = numpy.squeeze(numpy.asarray(pjiN.todense()))
            piiN = self.power_mats[self.nstep-i][trajectory[i-1]]
            piiN = numpy.squeeze(numpy.asarray(piiN.todense()))
            if piiN > 0:
                p = pjiN*pij/piiN
                p[trajectory] = 0.
                p[list(directVisitedNeighbours)] *= 1e-18
                if i < self.nstep -1:
                    p[self.x_stop] = 0.
                p /= p.sum()
                trajectory.append(numpy.random.choice(n_states, p=p))
            else:
                trajectory = []
                break
            neighbors = numpy.where(pij != 0)[0]
            directVisitedNeighbours.update(neighbors)
        return trajectory
