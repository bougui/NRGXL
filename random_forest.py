#!/usr/bin/env python
# -*- coding: UTF8 -*-

# Author: Guillaume Bouvier -- guillaume.bouvier@pasteur.fr
# https://research.pasteur.fr/en/member/guillaume-bouvier/
# 2017-10-19 08:58:01 (UTC+0200)

import numpy
from sklearn.ensemble import RandomForestClassifier
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
import sys
import math

def C(n, k):
    f = math.factorial
    return f(n)/(f(k)*f(n-k))

def p_value(n, k):
    return C(n,k)/float(2**n)

class RF(object):
    """
    Random Forest classifier
    """
    def __init__(self, xls):
        """
        • xls: list of crosslink objects
        """
        # Remove empty path:
        select = [len(xl.coords)>0 for xl in xls]
        self.xls = numpy.asarray(xls)[select]
        self.descriptors, self.is_xl = self._get_descriptors()

    def _get_descriptors(self):
        """
        Compute the descriptors for the random forest
        """
        descriptors = []
        is_xl = []
        print "Get descriptors: "
        for progress, xl in enumerate(self.xls):
            sys.stdout.write("%d/%d\r"%(progress+1, len(self.xls)))
            sys.stdout.flush()
            xl.descriptor = []
            xl.descriptor.extend([xl.min_density, xl.volum_sampling,
                                  xl.eucDist, numpy.mean(xl.curvatures), xl.rmsd])
            xl.descriptor.extend(self._get_energies(xl))
            descriptors.append(xl.descriptor)
            is_xl.append(int(xl.expcross))
        descriptors = numpy.asarray(descriptors)
        is_xl = numpy.asarray(is_xl)
        descriptors = preprocessing.scale(descriptors)
        return descriptors, is_xl

    def _get_energies(self, xl):
        """
        Get the CNS energies for a given xl
        """
        energylist = ["total", "bond", "angle", "impr", "dihe", "vdw", "elec",
                      "vdw_XL", "elec_XL", "vdw_inter", "elec_inter"]
        energies = []
        for energy_term in energylist:
            index = xl.energy['total'].argmin()
            energies_ = xl.energy[energy_term][index]
            energies.append(energies_)
        return energies

    def get_sets(self):
        """
        Get balanced set of descriptors for training and testing
        """
        desc = self.descriptors
        is_xl = self.is_xl
        X_train_1, X_test_1, y_train_1, y_test_1 = train_test_split(desc[is_xl == 1],
                                                                    is_xl[is_xl == 1], test_size=0.2)
        X_train_0, X_test_0, y_train_0, y_test_0 = train_test_split(desc[is_xl==0],
                                                                    is_xl[is_xl==0], test_size=0.2)

        r_train_0 = numpy.random.choice(y_train_0.size, size=y_train_1.size, replace=False)
        r_test_0 = numpy.random.choice(y_test_0.size, size=y_test_1.size, replace=False)
        X_train_0 = X_train_0[r_train_0]
        X_test_0 = X_test_0[r_test_0]
        y_train_0 = y_train_0[r_train_0]
        y_test_0 = y_test_0[r_test_0]

        X_train = numpy.r_[X_train_0, X_train_1]
        X_test = numpy.r_[X_test_0, X_test_1]
        y_train = numpy.r_[y_train_0, y_train_1]
        y_test = numpy.r_[y_test_0, y_test_1]

        r_train = numpy.random.choice(y_train.size, size=y_train.size, replace=False)
        r_test = numpy.random.choice(y_test.size, size=y_test.size, replace=False)

        X_train = X_train[r_train]
        X_test = X_test[r_test]
        y_train = y_train[r_train]
        y_test = y_test[r_test]
        self.n = len(y_test)
        return X_train, y_train, X_test, y_test

    def run(self, nrep=100):
        """
        • nrep: number of replicates to run
        """
        scores = []
        feature_importances = []
        for i in range(nrep):
            sys.stdout.write("%d/%d\r"%(i+1, nrep))
            sys.stdout.flush()
            X_train, y_train, X_test, y_test = self.get_sets()
            clf = RandomForestClassifier(n_estimators=100)
            clf.fit(X_train, y_train)
            y_pred = clf.predict(X_test)
            scores.append((y_test == y_pred).sum()/float(len(y_test)))
            feature_importances.append(clf.feature_importances_)
        self.scores = numpy.asarray(scores)
        self.feature_importances = numpy.asarray(feature_importances)

    def get_score(self):
        """
        Get the average score, the corresponding std and p-value
        """
        mu = self.scores.mean()
        sigma = self.scores.std()
        self.k = int(numpy.ceil(mu*self.n))
        return mu, sigma, p_value(self.n, self.k) 
