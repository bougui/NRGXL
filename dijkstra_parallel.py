#!/usr/bin/env python

"""
This program aims to lunch in parallel the program path_Class_scipyDijks2.py to obtain the shortest path of a list
of couple of residues
"""

import os
import numpy
import pdb
from mpi4py import MPI
COMM = MPI.COMM_WORLD
SIZE = COMM.Get_size() # Number of CPUS
RANK = COMM.Get_rank()
import numpy
import path_Class_scipyDijks2 as path_Class
import sys

"""
Declare the variables in all the cores
"""
inputname = sys.argv[1]
imputindex = sys.argv[2]
threshold = int(sys.argv[3])
voidRelevance = 0.00000000001
gridCellLength = 2
grids = {}

outputfile = open("lysineDistancesParallel_" + str(threshold) +'_' + str(gridCellLength) + ".csv", "a")


atom_types, aa_list, chain_ids, resids, coord, is_ca_trace = pdb.read_pdb(inputname)

"""
Build the list of jobs and the dictionary of the data. To do that we re going to use just one core and then 
broadcast the data to the others
"""
if RANK == 0:
    if not os.path.exists('output'):
        os.makedirs('output')
    imputfile = open(imputindex, "r")
    count=0
    datadic = {}
    for line in imputfile:
        parameters = line.split()
        data = [int(parameters[0]),int(parameters[1]),str(parameters[2]),str(parameters[3])]
        datadic[count] = data
        count += 1
    njobs=len(list(datadic.keys()))
    k = int(numpy.ceil(float(njobs) / SIZE))
    job_array = list(range(njobs))
    job_array.extend([None, ] * (k * SIZE - njobs))  # Add None values if not a multiple of SIZE
    job_array = numpy.asarray(job_array).reshape(k, SIZE)
    imputfile.close()
else:
    job_array = None
    datadic = {}

job_array = COMM.bcast(job_array, root=0)
datadic = COMM.bcast(datadic, root=0)


"""
Make the grid in just one core
"""
if RANK == 0:
    grid, grids = path_Class.get_grid(gridCellLength, voidRelevance, coord, threshold, grids)
else:
    grid = None
    grids = None

grid = COMM.bcast(grid, root=0)
grids = COMM.bcast(grids, root=0)



"""
Iterate through the list of jobs scattering them through the different cores.
"""
for job_ids in job_array:
    job_id = COMM.scatter(job_ids, root=0)
    if job_id is not None:
        pathLength, path, pathAtoms, finish_time = path_Class.shortest_path(gridCellLength, datadic[job_id][0], datadic[job_id][1], datadic[job_id][2], datadic[job_id][3] , atom_types, aa_list, chain_ids, resids, coord, grids,grid, threshold)
        outputfile.write('%d;%d;%s;%s;%f\n' %
                         (datadic[job_id][0], datadic[job_id][1], datadic[job_id][2], datadic[job_id][3], pathLength))

        print ("rank %d: job %d / %d" % (RANK, job_id,job_array.size))
    COMM.barrier()

outputfile.close()
