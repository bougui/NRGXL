An HOWTO on how to use NRGXL is available as a 
[Jupyter Notebook](https://colab.research.google.com/drive/14XxeXCmU3oJoBdjVkDzKiHJkj2scyXy2)

For changing the crosslinker type, an [HOWTO](https://colab.research.google.com/drive/10s4zl8HjPYN9dq4ewakGrk4Jesh2NDiX)
is proposed for using NRGXL to compute a cross-linker conformation for 
DSSO crosslinker. This can be adapted for any crosslinker topology.