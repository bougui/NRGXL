#!/usr/bin/env python
# -*- coding: UTF8 -*-

# Author: Guillaume Bouvier -- guillaume.bouvier@pasteur.fr
# https://research.pasteur.fr/en/member/guillaume-bouvier/
# 2017-10-05 09:24:22 (UTC+0200)

import sys
import os
import subprocess
import shutil
import numpy
import scipy.spatial
import scipy.sparse
import collections
import copy
from TEMPy.StructureParser import PDBParser
from TEMPy.StructureBlurrer import StructureBlurrer
import TEMPy.MapParser as MapParser
from TEMPy.ProtRep_Biopy import BioPyAtom
from TEMPy import EMMap
import markov_chain_bridge
import PDBparser as PDBwriter
import PCA
import tempfile
import sklearn.mixture as mixture

CNS_EXEC = "/home/bougui/source/CNS/cns_solve_1.21/intel-x86_64bit-linux/bin/cns"
CNS_FILES = os.environ['CNS_FILES']#"/home/bougui/lib/NRGXL/CNS"

def get_transition_matrix(dmap, beta=50., func=lambda x, dmap, beta: numpy.exp(-beta*(dmap - x)), normalize=True):
    """
    The probabilities are computed with: exp(-beta*(dj-di)) with dj and di the
    densities in cells i and j
    • func: function to compute the probabilities or weights
    """
    nx, ny, nz = dmap.shape
    # transition_matrix_data = numpy.zeros((numpy.prod(dmap.shape), 26))
    transition_matrix = scipy.sparse.coo_matrix((numpy.prod(dmap.shape),) * 2)
    transition_matrix.data = numpy.zeros(nx * ny * nz * 26)
    transition_matrix.row = numpy.zeros(nx * ny * nz * 26, dtype=int)
    transition_matrix.col = numpy.zeros(nx * ny * nz * 26, dtype=int)
    mgrid = numpy.asarray(numpy.meshgrid(*[numpy.arange(_) \
                                           for _ in (nx, ny, nz)], indexing='ij'))
    # Matrix that the give the rows in the transition matrix
    rowmat = numpy.ravel_multi_index(mgrid, (nx, ny, nz))
    n = nx * ny * nz
    ind = -1
    for i in (-1, 0, 1):
        for j in (-1, 0, 1):
            for k in (-1, 0, 1):
                if (i, j, k) != (0, 0, 0):
                    ind += 1
                    # transition_matrix_data[:, ind] = (1 / (dmap*move(dmap, (i, j, k)))).flatten()
                    # Matrix that the give the columns in the transition matrix
                    colmat = numpy.copy(mgrid)
                    colmat[:, 1:-1, 1:-1, 1:-1] += numpy.asarray((i, j, k))[:, None, None, None]
                    colmat = numpy.ravel_multi_index(colmat, (nx, ny, nz))
                    #transition_matrix.data[ind * n:(ind + 1) * n] = (1 / (dmap * move(dmap, (i, j, k)))).flatten()
                    transition_matrix.data[ind * n:(ind + 1) * n] = ( func(move(dmap, (i, j, k)), dmap, beta)).flatten()
                    transition_matrix.row[ind * n:(ind + 1) * n] = rowmat.flatten()
                    transition_matrix.col[ind * n:(ind + 1) * n] = colmat.flatten()
    transition_matrix = transition_matrix.tocsr()
    if normalize:
        # Normalize the transition matrix
        sq = numpy.squeeze(numpy.asarray(1 / transition_matrix.sum(axis=0)))
        diagsparse = scipy.sparse.csr_matrix((sq, (numpy.arange(n), numpy.arange(n))), shape=(n, n))
        transition_matrix = transition_matrix.dot(diagsparse)
    return transition_matrix

def get_density(pdb, resolution=4.4, mrc_ref=None):
    """
    Compute a density from a pdb file.
    • pdb: PDB filename
    • resolution: Resolution of the density map (default=6.16)
    • mrc_ref: Optionnal. To build a Map with dimensions based on the
    protein, or a Map instance to be used as a template.
    Remark: In the TEMPy source code for gaussian_blur the grid cell size is
    calculated as min(resolution/4., 3.5). Therefore to obtain a grid cell size
    of 1.1 A (the average length in all directions of the grid space is about
    the C-C bond length (1.54)) a resolution of 4.4 should be
    used.

    Returns a structure object and a map object
    """
    structure = PDBParser.read_PDB_file('protein', pdb)
    blurrer = StructureBlurrer()
    if mrc_ref is not None:
        map_ref = MapParser.MapParser.readMRC(mrc_ref)
    else:
        map_ref = None
    density_map = blurrer.gaussian_blur(structure, resolution, densMap=map_ref)
    return structure, density_map

def move(M, direction):
    """
    Get the map to move to the given direction.
    Returns: an array with the same dimensions as M
    To get the respective transition just do:
    1/(M*move(M, direction))
    M: density_map
    direction: Tuple given the direction: (1,0,0), (0,1,0), (0,0,1), (-1,0,0),
    ..., (0,0,-1)
    """
    nx, ny, nz = M.shape
    if direction[0] == -1:
        # Up
        M = numpy.concatenate( (-numpy.ones((1, ny, nz))*numpy.inf, M[:-1]), axis=0)
    if direction[0] == 1:
        # Down
        M = numpy.concatenate( (M[1:], -numpy.ones((1, ny, nz))*numpy.inf), axis=0)
    if direction[1] == -1:
        # Left
        M = numpy.concatenate( (-numpy.ones((nx, 1, nz))*numpy.inf, M[:,:-1,:]), axis=1)
    if direction[1] == 1:
        # Right
        M = numpy.concatenate( (M[:,1:,:], -numpy.ones((nx, 1, nz))*numpy.inf), axis=1)
    if direction[2] == -1:
        # Front
        M = numpy.concatenate( (-numpy.ones((nx, ny, 1))*numpy.inf, M[:,:,:-1]), axis=2)
    if direction[2] == 1:
        # Back
        M = numpy.concatenate( (M[:,:,1:], -numpy.ones((nx, ny, 1))*numpy.inf), axis=2)
    return M

def normalize(matrix):
    """
    Normalize the given array such as the minimum value is the minimal non zero
    positive element of the array and that the maximum value is one
    """
    matrix -= matrix.min()
    matrix += numpy.min(matrix[numpy.nonzero(matrix)])
    matrix[numpy.isinf(matrix)] = matrix[~numpy.isinf(matrix)].max()
    #matrix /= matrix.max()
    return matrix

def read_mrc(mrcfilename):
    """
    Read the given MRC file
    """
    return MapParser.MapParser.readMRC(mrcfilename)

def read_pdb(pdbfilename):
    """
    Read the given PDB file
    """
    return PDBParser.read_PDB_file('protein', pdbfilename)

def LJ(distance, radius=1.908, well_depth=0.1094):
    """
    6-9 (and not 6-12) Lennard jones potential using vdw_AMBER_parm99.defn parameter file from
    UCSF dock program
    The default parameters are for the Carbon sp3.
    """
    return well_depth*( (2*radius/distance)**9 - 2*(2*radius/distance)**6 )

def get_gmm(data, max_components=None):
    """
    Get the best GMM from the 1D data using Bayesian information criterion
    • max_components: Maximum number of components to test. If None,
    max_components is set to the number of elements in data.
    """
    data = data[:, None]
    bics = []
    if max_components is None:
        max_components = data.shape[0]
    # Step to get around 10 GMM to test:
    step_size = numpy.int(numpy.ceil(data.shape[0]/10.))
    n_list = range(1, max_components+1, step_size)
    gmms = []
    for n_components in n_list:
        gmm = mixture.GaussianMixture(n_components=n_components)
        gmm.fit(data)
        bics.append(gmm.bic(data))
        gmms.append(gmm)
    ind = numpy.argmin(bics)
    return gmms[ind]

class Density(object):
    """
    A class to store atomic density
    """
    def __init__(self, density_map, structure=None):
        """
        • density_map: A density map object as returns by get_density or a
        string giving the MRC filename containing the density map
        • structure: (optionnal) A structure object as returns by get_density
        or a string giving the PDB filename containing the structure
        """
        self.density_map = density_map
        if isinstance(self.density_map, basestring):
            self.density_map = read_mrc(self.density_map)
        self.structure = structure
        if isinstance(self.structure, basestring):
            self.structure = read_pdb(self.structure)
        self.x_origin = self.density_map.x_origin()
        self.y_origin = self.density_map.y_origin()
        self.z_origin = self.density_map.z_origin()
        self.x_size = self.density_map.x_size()
        self.y_size = self.density_map.y_size()
        self.z_size = self.density_map.z_size()
        self.cell_size = self.density_map.apix
        # Ensure that there is only positive values in density map
        dmap = self.density_map.getMap()
        self.density_map.fullMap = normalize(dmap)
        #self.transition_matrix = self.get_transition_matrix()

    def write_mrc(self, outfilename='density.mrc'):
        """
        Write the map to a MRC file with the given outfilename
        """
        self.density_map.write_to_MRC_file(outfilename)

    def get_atom(self, chain, resid, atom_name):
        """
        Get the coordinates of the given selection
        The atom is defined as:
        • chain: the chain id
        • resid: the residue id
        • atom_name: the name of the atom ('NZ', 'CA', ...)
        """
        chain = self.structure.get_chain(chain)
        residue = chain.get_residue(resid)
        atom = residue.atomList[numpy.asarray([_.atom_name for _ in \
                                residue.atomList]) == atom_name][0]
        return atom

    def get_atom_density(self, chain, resid, atom_name):
        """
        Get the coordinates and density value of the grid point in a density
        map closest to the given atom. Return 0 if atom is outside of map.
        The atom is defined as:
        • chain: the chain id
        • resid: the residue id
        • atom_name: the name of the atom ('NZ', 'CA', ...)
        Returns:
            • The grid coordinates: (x, y, z)
            • The density at that point: float
        """
        atom = self.get_atom(chain, resid, atom_name)
        x_grid, y_grid, z_grid, mass = atom.map_grid_position(self.density_map)
        grid_coords = (x_grid, y_grid, z_grid)
        density = self.density_map.fullMap[grid_coords]
        return grid_coords, density

    def get_cell(self, coords):
        """
        Given a coordinates return its cell
        """
        x_pos = int(round((coords[0]-self.x_origin)/self.cell_size))
        y_pos = int(round((coords[1]-self.y_origin)/self.cell_size))
        z_pos = int(round((coords[2]-self.z_origin)/self.cell_size))
        return x_pos, y_pos, z_pos

    def get_coords(self, cell):
        """
        Given a cell returns its coordinates
        """
        x = cell[0] * self.cell_size + self.x_origin
        y = cell[1] * self.cell_size + self.y_origin
        z = cell[2] * self.cell_size + self.z_origin
        return numpy.asarray([x, y, z])

class XL(object):
    """
    A class to store a crosslink
    """
    def __init__(self, density, atom1, atom2, n_sample=20, beta=2.5,
                 atoms = ('CB1','CG1','CD1','CE1','NZ1','CO1','CX1','CX2','CX3','CX4','CX5','CX6','CO2','NZ2','CE2','CD2','CG2','CB2'),
                 xlres='BSX'):
        """
        • density: density object (given by Density class)
        • atom1: (chain, resid, atom_name) ; e.g. ('A', 182, 'NZ')
        • atom2: (chain, resid, atom_name) ; ...
        • path_length: length of the Markov Chain Bridge path in grid cell
        points
        • xlres: residue name for the crosslinker
        """
        self.atom1 = atom1
        self.atom2 = atom2
        self.xlres = xlres
        atom1 = density.get_atom(*atom1)
        atom2 = density.get_atom(*atom2)
        self.path_length = len(atoms)
        self.n_sample = n_sample
        self.beta = beta
        self.atoms = atoms
        self.atoms_branched = []
        # Coordinates of the atom 1 and 2
        self.coords1 = numpy.asarray([atom1.x, atom1.y, atom1.z])
        self.coords2 = numpy.asarray([atom2.x, atom2.y, atom2.z])
        # Compute the box
        self.apix = density.density_map.apix
        self.get_box(density)
        # Compute a KD Tree for protein atomic coordinates inside the box
        self.kdtree, self.prot_coords = self.get_kdtree(density)
        # Coordinates in grid cells of atom 1 and 2
        cell1 = self.get_cell(self.coords1)
        self.cell1 = numpy.asarray(cell1)
        cell2 = self.get_cell(self.coords2)
        self.cell2 = numpy.asarray(cell2)
        # Compute the crosslink
        self.crosslink(density)
        # To store the positions of the oxygen atoms of the crosslink after CNS
        # minization
        self.coords_branched = None
        self._sasd = None
        # To store the coordinates of the flexible protein shell when needed.
        self.shell_atom_types = []
        self.shell_coords = []
        self.shell_resnames = []
        self.shell_resids = []
        self.shell_chainids = []

    def get_coords(self, cell):
        """
        Given a cell returns its coordinates
        """
        x = cell[0] * self.apix + self.origin[0]
        y = cell[1] * self.apix + self.origin[1]
        z = cell[2] * self.apix + self.origin[2]
        return numpy.asarray([x, y, z])

    def get_cell(self, coords):
        """
        Given a coordinates return its cell
        """
        x_pos = int(round((coords[0]-self.origin[0])/self.apix))
        y_pos = int(round((coords[1]-self.origin[1])/self.apix))
        z_pos = int(round((coords[2]-self.origin[2])/self.apix))
        # If the point is outside of the box (e.g. after minimization) find the
        # nearest point in the box...
        nx, ny, nz = self.box.shape
        if x_pos < 0:
            x_pos = 0
        if y_pos < 0:
            y_pos = 0
        if z_pos < 0:
            z_pos = 0
        if x_pos >= nx:
            x_pos = nx - 1
        if y_pos >= ny:
            y_pos = ny - 1
        if z_pos >= nz:
            z_pos = nz -1
        return x_pos, y_pos, z_pos

    def get_box(self, density):
        """
        Get the minimal box
        """
        midcell = density.get_cell((self.coords1 + self.coords2)/2.)
        self.midcell = midcell
        # Maximum path length :
        dmean = numpy.mean((self.apix, self.apix*numpy.sqrt(2), self.apix*numpy.sqrt(3)))
        dp = self.path_length * dmean
        # Distance between residues :
        dr = numpy.linalg.norm(self.coords2 - self.coords1)
        # normal vector:
        if dp < dr:
            normal = 0
        else:
            normal = .5*numpy.sqrt(dp**2-dr**2)
        # residual length:
        r_length = (dp - dr)/2
        v = ((self.coords2-self.coords1) / dr) * (2*r_length+dr)
        # unit vectors:
        ux = (density.get_coords([1,0,0])-density.get_coords([0,0,0]))/self.apix
        uy = (density.get_coords([0,1,0])-density.get_coords([0,0,0]))/self.apix
        uz = (density.get_coords([0,0,1])-density.get_coords([0,0,0]))/self.apix
        alpha = numpy.arccos(v.dot(ux) / (2*r_length+dr))
        beta = numpy.arccos(v.dot(uy) / (2*r_length+dr))
        gamma = numpy.arccos(v.dot(uz) / (2*r_length+dr))
        # size of the box
        lx = numpy.max((numpy.abs(v.dot(ux)), 2*numpy.abs(normal*numpy.sin(alpha))))
        ly = numpy.max((numpy.abs(v.dot(uy)), 2*numpy.abs(normal*numpy.sin(beta))))
        lz = numpy.max((numpy.abs(v.dot(uz)), 2*numpy.abs(normal*numpy.sin(gamma))))
        # convert the length to number of cells
        lx = int(numpy.ceil(lx/self.apix))
        ly = int(numpy.ceil(ly/self.apix))
        lz = int(numpy.ceil(lz/self.apix))
        xstart = midcell[0] - int(numpy.ceil(lx/2.))
        xstop = midcell[0] + int(numpy.ceil(lx/2.)) + 1
        ystart = midcell[1] - int(numpy.ceil(ly/2.))
        ystop = midcell[1] + int(numpy.ceil(ly/2.)) + 1
        zstart = midcell[2] - int(numpy.ceil(lz/2.))
        zstop = midcell[2] + int(numpy.ceil(lz/2.)) + 1
        self.box = density.density_map.fullMap[zstart:zstop, ystart:ystop, xstart:xstop]
        self.box = numpy.moveaxis(self.box, (0, 2), (2, 0))
        self.origin_box = numpy.asarray((xstart, ystart, zstart))
        self.origin = density.get_coords([xstart, ystart, zstart])
        #print "Size of the box: %d %d %d"%self.box.shape

    def crosslink(self,density):
        """
        Compute a MarkovChain bridge between:
        """
        self.transition_matrix = get_transition_matrix(self.box, beta=self.beta)
        path_start = self.cell1
        path_start = numpy.ravel_multi_index(path_start, self.box.shape)
        path_stop = self.cell2
        path_stop = numpy.ravel_multi_index(path_stop, self.box.shape)
        self.coords = []
        self.paths = []
        mcb = markov_chain_bridge.MCB(self.transition_matrix, self.path_length, path_stop)
        for sample in range(self.n_sample):
            path = mcb.mc_bridge(path_start) # Path in small grid box with 1D ravel index
            path = [numpy.unravel_index(_, self.box.shape) for _ in path] # Path in small grid box
            if len(path) == self.path_length:
                self.paths.append(path)
                path = [self.get_coords(_) for _ in path] # Path in Euclidean space
                self.coords.append(numpy.asarray(path))
            else:
                print "WARNING: Cannot compute path %d"%sample

    def get_shortest_path(self, beta=25.):
        """
        Compute the shortest path between the lysine headgroups for the minimal energy path
        """
        adjmat = get_transition_matrix(self.box, beta=beta)
        adjmat.data = -numpy.log(adjmat.data)
        adjmat.data -= adjmat.data.min()
        start, stop = self.cell1, self.cell2
        start = numpy.ravel_multi_index(start, self.box.shape)
        stop = numpy.ravel_multi_index(stop, self.box.shape)
        dist_matrix, predecessors = scipy.sparse.csgraph.dijkstra(adjmat,
                                                                  return_predecessors=True,
                                                                  indices=(start, stop), directed=True)
        path = []
        path.append(stop)
        prev = stop
        while prev != start:
            prev = predecessors[0][prev]
            path.append(prev)
        path = path[::-1]
        path = [numpy.unravel_index(_, self.box.shape) for _ in path] # Path in small grid box
        coords = numpy.asarray([self.get_coords(_) for _ in path]) # Path in Euclidean space
        sasd = numpy.linalg.norm(coords[1:] - coords[:-1], axis=1).sum() # Solvent Accessible Surface Distance
        self.shortest_path = path
        self.coords_shortest_path = numpy.asarray(coords)
        self._sasd = sasd

    @property
    def sasd(self):
        """
        Solvent Accessible Surface Distance
        """
        if self._sasd is None:
            try:
                self.get_shortest_path()
            except IndexError:
                self._sasd = numpy.inf
        return self._sasd

    @property
    def densities(self):
        """
        Densities along the sampled paths
        :return: 
        """
        densities = []
        for path in self.paths:
            try:
                densities.append([self.box[_] for _ in path])
            except IndexError:
                print "WARNING: Path outside the box"
        return numpy.asarray(densities)

    @property
    def density_sum(self):
        """
        Sum of densities over each sample path
        """
        return self.densities.sum(axis=1)

    @property
    def mean_densities(self):
        """
        Average density along the steps of the sampled paths
        :return: 
        """
        return self.densities.mean(axis=0)

    @property
    def std_densities(self):
        """
        STD  along the steps of the sampled paths
        :return: 
        """
        return self.densities.std(axis=0)

    @property
    def mean_path(self):
        """
        Compute the mean crosslink
        :return: numpy.ndarray
        """
        return numpy.asarray(self.coords).mean(axis=0)

    @property
    def min_density_path(self):
        """
        Get the coordinates of the path with minimal density
        """
        return self.coords[numpy.argmin(self.density_sum)]

    @property
    def min_vdw_path(self):
        """
        Get the coordinates of the path with minimal VDW energy
        """
        return self.coords[numpy.argmin(self.lj)]

    @property
    def min_density(self):
        """
        Get the minimum density over the sampled paths 
        :return: 
        """
        return min(self.density_sum)

    @property
    def rmsd(self):
        """
        Get the Root-mean-square deviation over all the sampled paths
        :return: 
        """
        return numpy.sqrt(numpy.mean(((self.coords - self.mean_path) ** 2)))

    @property
    def length_mean_path(self):
        """
        Get the length of the mean path
        :return: 
        """
        return numpy.sqrt((numpy.diff(numpy.asarray(zip(self.mean_path,
                                        self.mean_path[1:])), axis=1)**2).sum(axis=-1)).sum()

    @property
    def length_paths(self):
        """
        Get the length of all the sampled paths
        :return: 
        """
        lengths=[]
        for coord in self.coords:
            lengths.append(numpy.sqrt((numpy.diff(numpy.asarray(zip(coord,
                                        coord[1:])), axis=1) ** 2).sum(axis=-1)).sum())
        return lengths

    @property
    def curvatures(self):
        """
        Get the curvature per each step of each sampled path
        :return: 
        """
        curvatures = []
        for coord in self.coords:
            curvature =numpy.linalg.norm(numpy.gradient(numpy.gradient(coord,
                                         self.apix)[0])[0], self.apix, axis=1)
            curvatures.append(curvature)
        return numpy.asarray(curvatures)

    @property
    def volum_sampling(self):
        """
        Get a measure of volume counting all the visited cells during the sampling process of paths
        :return: 
        """
        cell_set = set()
        for path in self.paths:
            for step in path:
                cell_set.add(step)
        return len(cell_set) * (self.apix ** 3)

    @property
    def eucDist(self):
        """
        Get the Euclidian distance between the two CB of the residues
        :return: 
        """
        return numpy.linalg.norm(numpy.asarray(self.coords1) - numpy.asarray(self.coords2))

    @property
    def pca(self):
        """
        Compute the PCA on the coordinates of the paths.
        pca.eigh returns the eigenvalues and the eigenvectors sorted by
        decreasing eigenvalues.
        """
        return PCA.PCA(self.coords)

    def write_all_path(self, outfilename='path.pdb', multimodel=True,
    write_oxygen=False):
        """
        Write the given path to pdb
        """
        if len(self.shell_coords) == 0: # No protein flexibility
            coords = self.coords
            atoms = self.atoms
            connect=None
            if write_oxygen:
                coords_branched = self.coords_branched
                for i, (coords_, coords_branched_) in enumerate(zip(coords, coords_branched)):
                    coords[i] =  numpy.r_[coords_, coords_branched_]
                atoms = atoms+tuple(self.atoms_branched)
            n_atoms = len(atoms)
            n_models = len(coords)
            atoms = [atoms, ]*n_models
            chain_ids = [['Z', ]*n_atoms, ]*n_models
            resnames = [[self.xlres, ]*n_atoms, ]*n_models
            resids = [[999, ]*n_atoms, ]*n_models
        else:
            coords = []
            atoms = []
            chain_ids = []
            resnames = []
            resids = []
            for i, coords_ in enumerate(self.coords):
                coords_, atoms_, resnames_, chain_ids_, resids_ = self._merge_xl_shell(i)
                coords.append(coords_)
                atoms.append(atoms_)
                chain_ids.append(chain_ids_)
                resnames.append(resnames_)
                resids.append(resids_)
            n_atoms = len(self.atoms)
        if multimodel:
            PDBwriter.write_pdb(coords, outfilename=outfilename, multimodel=True, atoms=atoms, chain_ids=chain_ids, resnames=resnames, resids=resids, write_conect=False)
        else:
            for i, coord in enumerate(coords):
                PDBwriter.write_pdb(coord, outfilename="path_%d.pdb"%(i+1), atoms=atoms[i], chain_ids=chain_ids[i], resnames=resnames, resids=resids, write_conect=False)

    def _merge_xl_shell(self, i):
        """
        Merge the coordinates from the XL and from the protein shell
        """
        coords = numpy.r_[self.coords[i], self.shell_coords[i]]
        atoms = numpy.r_[self.atoms, self.shell_atom_types[i]]
        n_xl = self.coords[i].shape[0]
        n_sh = self.shell_coords[i].shape[0] 
        chain_ids = numpy.r_[['Z', ]*n_xl, self.shell_chainids[i]]
        resids = numpy.r_[[999, ]*n_xl, self.shell_resids[i]]
        resnames = numpy.r_[[self.xlres, ]*n_xl, self.shell_resnames[i]]
        return coords, atoms, resnames, chain_ids, resids

    def write_path(self, i, outfilename, write_oxygen=False):
        """
        Write the given path index to PDB file: outfilename
        """
        if len(self.shell_coords) == 0: # No protein flexibility
            if not write_oxygen:
                PDBwriter.write_pdb(self.coords[i], outfilename=outfilename, atoms=self.atoms, hetatm=True, resnames=[self.xlres,]*len(self.atoms),
                                    write_conect=False)
            else:
                coords = self.coords[i].copy()
                if self.coords_branched is not None:
                    coords_branched = self.coords_branched[i].copy()
                    coords = numpy.r_[coords, coords_branched]
                    connect = zip(range(1,18), range(2,19))
                    connect.extend([(6, 19), (13 ,20)])
                    PDBwriter.write_pdb(coords, outfilename=outfilename,
                                        atoms=self.atoms+('O1', 'O2'),
                                        hetatm=True, connect=connect)
                else:
                    self.write_path(i, outfilename, write_oxygen=False)
        else: # A part of the protein is flexible, write that to file along with the XL
            coords, atoms, resnames, chain_ids, resids = self._merge_xl_shell(i)
            n_atoms = len(self.atoms)
            connect = zip(range(1,n_atoms),range(2,n_atoms+1))
            PDBwriter.write_pdb(coords, outfilename=outfilename, atoms=atoms, chain_ids=chain_ids, resnames=resnames, resids=resids,
                    connect=connect)

    def write_shortest_path(self, outfilename='shortest_path.pdb'):
        """
        Write the shortest path
        """
        coords = self.coords_shortest_path
        PDBwriter.write_pdb(coords, outfilename=outfilename)


    def write_mean_path(self, outfilename='meanpath.pdb'):
        """
        write self.mean in pdb
        :param outfilename: 
        :return: 
        """
        PDBwriter.write_pdb(self.mean_path, outfilename=outfilename, multimodel=False,atoms=self.atoms)

    def write_min_density_path(self, outfilename='minpath.pdb'):
        """
        write the min density path in pdb file
        """
        PDBwriter.write_pdb(self.min_density_path, outfilename=outfilename, multimodel=False, atoms=self.atoms)

    def write_min_vdw_path(self, outfilename='min_vdw_path.pdb'):
        """
        write the min VDW path in pdb file
        """
        PDBwriter.write_pdb(self.min_vdw_path, outfilename=outfilename, multimodel=False, atoms=self.atoms)

    def cxl(self, filename):
        """
        Read the minimized output PDB from CNS for the XL
        """
        # Read coordinates:
        atom_types, _, _, _, coords, _ = PDBwriter.read_pdb(filename)
        atom_types = numpy.asarray(atom_types)
        coords_min = []
        coords_branched = []
        self.atoms_branched = []
        for c, atom_type in zip(coords, atom_types):
            if atom_type in self.atoms:
                coords_min.append(c)
            else:
                self.atoms_branched.append(atom_type)
                coords_branched.append(c)
        coords_min = numpy.squeeze(coords_min)
        coords_branched = numpy.squeeze(coords_branched)
        path = [self.get_cell(_) for _ in coords_min]
        return coords_min, coords_branched, path

    def _read_minimized_shell(self, filename):
        """
        Read the minimized flexible shell conformation for the protein
        """
        # Read coordinates:
        # atom_types, aa_list, chain_ids, resids, coords, is_ca_trace
        atom_types, resnames, chainids, resids, coords, _ = PDBwriter.read_pdb(filename)
        atom_types = numpy.asarray(atom_types)
        return atom_types, resnames, resids, chainids, coords

    def cns_minimize(self, threshold=None, ntasks=2, flex_radius=0., k_harmonic=10.):
        """
        Use CNS to minimize the energy
        • ntasks: number of task to run in parallel
        • threshold: Energy threshold to put for the filtering. If None,
        compute a GMM to get an automatic threshold.
        • flex_radius: Radius in Angstrom for flexible protein (0 for no
        flexibility)
        • k_harmonic: harmonic restraint constant - for harmonically restrained
        atoms
        """
        n = len(self.paths)
        # Parallel list:
        k = int(numpy.ceil(float(n)/ntasks))
        job_array = range(n)
        job_array.extend([None, ]*(k*ntasks-n)) # Add None values if not a multiple of ntasks
        job_array = numpy.asarray(job_array).reshape(k, ntasks)
        tempdirs = []
        print "Minimizing energy:"
        for jobids in job_array:
            subprocesses = []
            for i in jobids:
                if i is not None:
                    sys.stdout.write("%d/%d\r"%(i+1, n))
                    sys.stdout.flush()
                    # Create a temporary directory for CNS files
                    tempdir = tempfile.mkdtemp(dir='/dev/shm')
                    tempdirs.append(tempdir)
                    shutil.copy("cns/prot_CNS.mtf", tempdir)
                    shutil.copy("cns/prot_CNS.pdb", tempdir)
                    self.write_path(i, outfilename="%s/cns_path_1.pdb"%tempdir)
                    shutil.copy("%s/protein_nohydrogen.top"%CNS_FILES, tempdir)
                    shutil.copy("%s/protein.param"%CNS_FILES, tempdir)
                    shutil.copy("%s/%s.mtf"%(CNS_FILES, self.xlres), tempdir)
                    with open('%s/flex.cns'%tempdir, 'w') as flexfile:
                        flexfile.write('define (\nflex_radius=%.2f;\nk_harmonic=%.2f;\n)'%(flex_radius, k_harmonic))
                    sp = subprocess.Popen(["%s < %s/minimize_xlink.inp > log.out"%(CNS_EXEC, CNS_FILES), ], cwd=tempdir, shell=True)
                    subprocesses.append(sp)
            for sp in subprocesses:
                sp.wait()
        energylist = ["total", "bond", "angle", "impr", "dihe", "vdw", "elec", "vdw_XL", "elec_XL", "vdw_inter", "elec_inter", "harmonic", "rms"]
        energies = []
        coords = []
        coords_branched = [] # Coordinates for oxygen atoms
        paths = []
        for tempdir in tempdirs:
            coords_, coords_branched_, path = self.cxl('%s/cns_path_1_min.pdb'%tempdir)
            # The double check below is mandatory as for some small radius the
            # protein shell can be empty, therefore no cns_path_1_min_shell.pdb
            # file exists.
            if flex_radius > 0. and os.path.exists('%s/cns_path_1_min_shell.pdb'%tempdir):
                shell_atom_types_, shell_resnames_, shell_resids_, shell_chainids_, shell_coords_ = self._read_minimized_shell('%s/cns_path_1_min_shell.pdb'%tempdir)
                self.shell_atom_types.append(shell_atom_types_)
                self.shell_resnames.append(shell_resnames_)
                self.shell_coords.append(shell_coords_)
                self.shell_resids.append(shell_resids_)
                self.shell_chainids.append(shell_chainids_)
            coords.append(coords_)
            coords_branched.append(coords_branched_)
            paths.append(path)
            output_energies = open('%s/energies.list'%tempdir)
            lines = output_energies.readlines()
            energies.append(numpy.float_(lines[1].split()[1:]))
            shutil.rmtree(tempdir)
        self.coords = coords
        self.paths = paths
        self.coords_branched = coords_branched
        energies = numpy.asarray(energies)
        self.energy = dict(zip(energylist, energies.T))
        print "... ... ... done"
        if len(self.energy) > 0:
            # Remove the harmonic energy from the total
            self.energy['total'] -= self.energy['harmonic']
            self._filter_xl(threshold=threshold)

    def _filter_xl(self, threshold=None):
        """
        Filter the XLs based on the CNS total energy of the minimized XL
        conformation
        • threshold: Energy threshold to put for the filtering. If None,
        compute a GMM to get an automatic threshold.
        """
        energies = self.energy['total']
        if threshold is None:
            # Compute the best GMM from the energies
            gmm = get_gmm(energies)
            # Get the energy threshold
            ind = gmm.weights_.argmax()
            mu = gmm.means_[ind]
            sigma = gmm.covariances_[ind][0,0]
            threshold = mu+3*sigma
        print "Filtering XL conformations (threshold=%.2f):"%threshold
        selection = energies < threshold
        # Filter the energy terms
        for term in self.energy:
            self.energy[term] = self.energy[term][selection]
        print "%.2f %% crosslinks filtered"%((1.-selection.sum()/float(len(self.paths)))*100)
        def apply_filter(inlist):
            return [_ for i, _ in enumerate(inlist) if selection[i]]
        # Filter the paths
        self.paths = apply_filter(self.paths)
        # Filter the coordinates
        self.coords = apply_filter(self.coords)
        self.coords_branched = apply_filter(self.coords_branched)
        # Filter the protein shell
        if len(self.shell_coords) > 0: # Protein flexibility
            self.shell_atom_types = apply_filter(self.shell_atom_types)
            self.shell_resnames = apply_filter(self.shell_resnames)
            self.shell_coords = apply_filter(self.shell_coords)
            self.shell_resids = apply_filter(self.shell_resids)
            self.shell_chainids = apply_filter(self.shell_chainids)

    def write_box(self, outfilename='box.mrc'):
        """
        Write the box to a MRC file
        :return: 
        """
        emmap = EMMap.Map(numpy.moveaxis(self.box, (0, 2), (2, 0)), self.origin, self.apix, outfilename)
        emmap.write_to_MRC_file(outfilename)

    def get_kdtree(self, density):
        """
        Compute the KD Tree for the atomic coordinates inside the box
        Return:
        - The kdtree
        - The protein atomic coordinates inside the box
        """
        nx, ny, nz = self.box.shape
        # Border of the box:
        c1 = self.get_coords([0, 0, 0])
        c2 = self.get_coords([nx, ny, nz])
        prot_coords = density.structure.get_pos_mass_list()[:, :-1]
        sel1 = (prot_coords > c1).all(axis=1)
        sel2 = (prot_coords < c2).all(axis=1)
        selection = numpy.logical_and(sel1, sel2)
        prot_coords = prot_coords[selection]
        kdtree = scipy.spatial.cKDTree(prot_coords) 
        return kdtree, prot_coords

    def get_neighbors(self, path, radius):
        """
        Get the neighboring protein atoms for a given path.
        • path: Coordinates of the path
        • radius: Distance threshold in Angstrom
        """
        index = self.kdtree.query_ball_point(path, radius)
        neighbors = [self.prot_coords[_] for _ in index]
        distances = [scipy.spatial.distance.cdist(path[i][None, :], _)[0] for i, _ in enumerate(neighbors)]
        # Flatten the results:
        index_flat = []
        for index_ in index:
            index_flat.extend(list(index_))
        distances_flat = []
        for distances_ in distances:
            distances_flat.extend(list(distances_))
        return numpy.asarray(index_flat), numpy.asarray(distances_flat)

    @property
    def d(self):
        """
        Interatomic distance between the crosslinker and the protein atoms
        around 5 A.
        """
        cutoff = 5.
        dlist = []
        for coords in self.coords:
            index, distance = self.get_neighbors(coords[5:-5], cutoff)
            dlist.append(distance)
        return dlist

    @property
    def min_d(self):
        """
        Minimal interatomic distance between the crosslinker and the protein
        atoms around 5 A. If no neighbors at that 5 A cutoff return the cutoff
        value (5 A).
        """
        cutoff = 5.
        dlist = []
        for coords in self.coords:
            index, distance = self.get_neighbors(coords[5:-5], cutoff)
            if len(distance) > 0:
                dlist.append(distance.min())
            else:
                dlist.append(cutoff)
        return dlist


    @property
    def lj(self):
        """
        Lennard jones potential for the paths (only for the crosslinker part)
        """
        cutoff = 5.
        potentials = []
        for coords in self.coords:
            index, distance = self.get_neighbors(coords[5:-5], cutoff)
            potentials.append(LJ(distance).sum())
        return numpy.asarray(potentials)
