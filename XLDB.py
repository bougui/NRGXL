#!/usr/bin/env python
# -*- coding: UTF8 -*-

# Author: Guillaume Bouvier -- guillaume.bouvier@pasteur.fr
# https://research.pasteur.fr/en/member/guillaume-bouvier/
# 2017-11-02 11:42:30 (UTC+0100)

import itertools
import sys
import os
import pickle
import numpy
import scipy.stats
import scipy.optimize
try:
    import seaborn as sns
except ImportError:
    print "seaborn not installed..."
import matplotlib.pyplot as plt
import sklearn.model_selection
from sklearn.ensemble import RandomForestClassifier

class Progress(object):
    def __init__(self, n):
        """
        • n: the total number of step
        """
        self.n = n
        self.p = 0
        self.i = 0

    def step(self):
        """
        Print the progress for step i (0-based) for a total step of n.
        • i: the index of the step (0-based)
        • p_prev: the previous progress percentage as return by the function
        """
        self.i += 1
        p = int((self.i)*100/self.n)
        if p > self.p:
            sys.stdout.write("%02d%% \r"%p)
            sys.stdout.flush()
        self.p = p

def get_cumulative(data, bins=None):
    """
    Cumulative distribution
    • data: one dimensional array or n-dimensional array
    • bins (optional): list of ordered thresholds
    """
    is_1d = False
    n_dim = len(data.shape)
    if n_dim == 1:
        data = data[None, :]
        is_1d = True
    n_data = data.shape[-1]
    if bins is None:
        bins = []
        for data_ in data:
            data_ = numpy.sort(data_)
            bins.append(data_)
    bins = numpy.asarray(bins)
    if len(bins.shape) == 1:
        bins = bins[None, :]
    counts = []
    for thresholds in itertools.product(*bins):
        count = (data <= numpy.asarray(thresholds)[:, None]).all(axis=0).sum()
        counts.append(count)
    counts = numpy.float_(counts)
    bins = numpy.squeeze(bins)
    if n_dim > 1:
        counts = counts.reshape(n_data, n_data)
    return bins, counts

def joinXLDB(xldb1,xldb2):
    xldb = XLDB()
    xldb1_list = xldb1.get_all()
    xldb2_list = xldb2.get_all()
    for xl in xldb1_list:
        xldb.add(xl.pdb,xl, filename=xl.filename, xl_key=xl.key)
    for xl in xldb2_list:
        xldb.add(xl.pdb,xl, filename=xl.filename, xl_key=xl.key)
    return xldb

def _get_accuracy(data, thresholds, expdata, right=True):
    """
    Get accuracy for the given data, threshold and experimental data
    accuracy = (TP+TN)/(TP+FN+TN+FP)
    """
    try:
        n_threshold = len(thresholds)
    except TypeError:
        thresholds = [thresholds, ]
        n_threshold = 1
    expdata = numpy.asarray(expdata)
    data = numpy.asarray(data)
    scores = []
    for threshold in thresholds:
        if right:
            selection = (data <= threshold)
        else:
            selection = (data >= threshold)
        score = (selection == expdata).sum() / float(len(expdata))
        scores.append(score)
    return numpy.squeeze(scores)

def _get_fpr(data, thresholds, expdata, right=True):
    """
    Fet the False Positive Rate
    """
    try:
        n_threshold = len(thresholds)
    except TypeError:
        thresholds = [thresholds, ]
        n_threshold = 1
    expdata = numpy.asarray(expdata)
    data = numpy.asarray(data)
    fpr = []
    for threshold in thresholds:
        if right:
            selection = (data <= threshold)
        else:
            selection = (data >= threshold)
        fpr.append(numpy.logical_and(selection, ~expdata).sum() / float((~expdata).sum()))
    return numpy.squeeze(fpr)

def _train_test_split(descriptors, expdata, ratio=.2):
    """
    Split the descriptors in two population given by ratio for a training set
    and a testing set.
    • descriptors: array of shape (n_samples, n_features)
    • expdata: target values of shape (n_samples, )
    """
    n_samples, n_features = descriptors.shape
    X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(descriptors, expdata, test_size=ratio)
    return X_train, X_test, y_train, y_test

class _XL(object):
    """
    Private XL object
    """
    def __init__(self, xl, pdbname):
        selected_attributes = ['energy', 'atom1', 'atom2', 'coords_O', 'expcross', 'coords', 'volum_sampling', 'eucDist', 'sasd']
        self.pdb = pdbname
        minimal_energy_index = xl.energy['total'].argmin()
        self.key = None # Store the xl key (e.g.: '71E171E')
        self.filename = None # Filename of the xl file containing a dictionnary of xl: {key: xl_object, ...}
        for key in  selected_attributes:
            if key in ['coords_O', 'coords']:
                self.__setattr__(key, xl.__getattribute__(key)[minimal_energy_index])
            elif key == 'energy':
                energies = xl.energy
                for energy_term in energies:
                    self.__setattr__('energy_%s'%energy_term, energies[energy_term][minimal_energy_index])
            else:
                self.__setattr__(key, xl.__getattribute__(key))
        self._pdb_instance = None

    @property
    def auc(self):
        """
        AUC
        """
        if self._pdb_instance is not None:
            auc = self._pdb_instance.auc
        else:
            auc = None
        return auc

    @property
    def n_xl(self):
        if self._pdb_instance is not None:
            n_xl = self._pdb_instance.n_xl
            if n_xl is None:
                n_xl = 0
        else:
            n_xl = None
        return n_xl

    @property
    def n_nxl(self):
        if self._pdb_instance is not None:
            n_nxl = self._pdb_instance.n_nxl
            if n_nxl is None:
                n_nxl = 0
        else:
            n_nxl = None
        return n_nxl

    @property
    def score(self):
        """
        accuracy score
        """
        if self._pdb_instance is not None:
            score = self._pdb_instance.score[0]
        else:
            score = None
        return score

class _PDB(object):
    """
    Object to store data related to a PDB
    """
    def __init__(self, name, filename=None):
        """
        • xls: list of crosslinks
        • name: PDB code or name
        • filename: name of the file with the given xl
        """
        self._xls = []
        self.name = name
        self.n_xl = None
        self.n_nxl = None
        self.threshold = None
        self.score = (None, None, None)
        self.auc = None
        self.filename = filename
        self.keys = [] # keys for the crosslinks

    def add_xl(self, xl, key=None):
        """
        Add an xl object to the list of crosslinks (self._xls)
        • key: key of the given xl
        """
        self._xls.append(xl)
        self.keys.append(key)

    @property
    def xls(self):
        """
        Get the list of crosslinks
        """
        xl_list = []
        for xl in self._xls:
            xl._pdb_instance = self
            xl_list.append(xl)
        return xl_list
        
    def print_data(self):
        print "%s (%d - %d XL): %.2f kcal/mol; %.2f +- %.2f (p-value=%.2g); AUC=%.2f"%(self.name, self.n_xl, self.n_nxl, self.threshold, self.score[0], self.score[1], self.score[2], self.auc)

class XLDB(object):
    """
    A class to store a database of crosslinks
    """
    def __init__(self):
        self.pdbs = []
        self.scores = None
        self.score = None
        self.thresholds = None
        self.random_scores = None

    def add(self, pdbname, xl, filename=None, xl_key=None):
        """
        Add the xl object for the given pdbname to the database
        • filename: name of the file with the given xl
        • xl_key: key of the given xl
        """
        if not xl.__class__.__name__ == '_XL':
            n_path = len(xl.paths)
            _xl = False
        else:
            n_path = 1
            _xl = True
        if n_path > 0:
            if pdbname not in self.pdbs:
                self.pdbs.append(str(pdbname)) # Ensure to have a string and not a numpy string.
            key = "_%s"%pdbname
            if not _xl:
                xl = _XL(xl, pdbname)
                xl.key = xl_key
                xl.filename = filename
            if not hasattr(self, key):
                _pdb = _PDB(pdbname, filename=filename)
                _pdb.add_xl(xl, key=xl_key)
                self.__setattr__(key, _pdb)
            else:
                _pdb = self.__getattribute__(key)
                _pdb.add_xl(xl, key=xl_key)

    def get_sub_db(self, selection):
        """
        Get a sub database from the given selection
        • selection: boolean vector with the same length as the number of
        crosslinks in the database
        """
        xls = self.get_all()
        xls = xls[selection]
        xldb = XLDB()
        for xl in xls:
            xldb.add(xl.pdb, xl, filename=xl.filename, xl_key=xl.key)
        return xldb

    def get_xls(self, pdbname):
        """
        Return the full list of full xl object for the given pdb
        """
        pdb = self.get_pdb(pdbname)
        with open(pdb.filename) as xl_file:
            xls = pickle.load(xl_file)
        return xls.values()

    def get_xl(self, _xl):
        """
        Return the full xl object for the given internal _xl object
        """
        with open(_xl.filename) as xl_file:
            xls = pickle.load(xl_file)
        xl = xls[_xl.key]
        xl.pdb = _xl.pdb
        return xl

    @property
    def features(self):
        # list of attributes:
        xl = self.get(list(self.pdbs)[0])[-1]
        attr = xl.__dict__.keys()
        # and attributes
        prop = [p for p in dir(_XL) if isinstance(getattr(_XL,p),property)]
        feature_list = attr
        feature_list.extend(prop)
        return feature_list

    def get_pdb(self, pdbname):
        """
        Return the respective pdb object
        """
        return self.__getattribute__('_%s'%pdbname)

    def get(self, pdbname, xled=None):
        """
        Get the list of XLs for the given pdbname
        • If xled is True: return only the XL for experimentally XLed data
        • If xled is False: return only the XL for not experimentally XLed data
        • Else return the XL for all the database
        """
        _pdb = self.get_pdb(pdbname)
        XL_list = numpy.asarray(_pdb.xls)
        if xled is not None:
            selection = self.get_values('expcross', pdb=pdbname)
            if xled is True:
                XL_list = XL_list[selection]
            else:
                XL_list = XL_list[~selection]
        return list(XL_list)

    def get_all(self, xled=None):
        """
        Get the list of all the XLs
        • If xled is True: return only the XL for experimentally XLed data
        • If xled is False: return only the XL for not experimentally XLed data
        • Else return the XL for all the database
        """
        XL_list = []
        for pdbname in self.pdbs:
            XL_list.extend(self.get(pdbname, xled=xled))
        return numpy.asarray(XL_list)

    def get_values(self, feature, xled=None, pdb=None):
        """
        Get the values for the given feature for the given pdb, else for all
        the database.
        Feature list is given by self.features
        • If xled is True: return only the values for experimentally XLed data
        • If xled is False: return only the values for not experimentally XLed data
        • Else return the values for all the database
        • pdb: plot the distribution for the given pdb or list of pdbs. Default
        is for the whole database.
        """
        if pdb is not None:
            if type(pdb) is str:
                pdb = [pdb, ]
            xls = []
            for pdb_ in pdb:
                xls.extend(self.get(pdb_, xled=xled))
        else:
            xls = self.get_all(xled=xled)
        features = [xl.__getattribute__(feature) for xl in xls]
        return numpy.asarray(features)

    def plot_distribution(self, feature, cumulative=False, pdb=None):
        """
        Plot the distribution of the given feature
        • feature: one of the feature as given by self.features
        • cumulative: wether to plot cumulative distribution
        • pdb: plot the distribution for the given pdb or list of pdbs. Default
        is for the whole database.
        """
        data_xled = self.get_values(feature, pdb=pdb, xled=True)
        data_noxled = self.get_values(feature, pdb=pdb, xled=False)
        # Minimal difference between consecutive sorted values for plotting
        # purpose:
        delta = numpy.r_[data_xled, data_noxled]
        delta = numpy.sort(delta)
        delta = numpy.diff(delta).min()
        if pdb is not None:
            pdblabel = str(pdb)
        else:
            pdblabel = "Whole DB"
        if not cumulative:
            sns.distplot(data_xled, hist=True, label="XL (%s)"%pdblabel)
            sns.distplot(data_noxled, hist=True, label="No XL (%s)"%pdblabel)
            xmin = min([min(data_xled), min(data_noxled)])
            xmax = numpy.max(data_xled)
        else:
            x_xled, counts_xled = get_cumulative(numpy.asarray(data_xled))
            x_noxled, counts_noxled = get_cumulative(numpy.asarray(data_noxled))
            x_xled = numpy.r_[x_xled[0]-delta, x_xled]
            counts_xled = numpy.r_[0, counts_xled]
            plt.plot(x_xled, counts_xled/len(x_xled), label="XL (%s)"%pdblabel)
            plt.plot(x_noxled, counts_noxled/len(x_noxled), label="No XL (%s)"%pdblabel)
            xmin = numpy.min(x_xled)
            xmax = numpy.max(x_xled)
        plt.legend()
        axes = plt.gca()
        axes.set_xlim([xmin, xmax])
        plt.xlabel(feature)
        plt.ylabel("DB ratio")

    def get_classification_values(self,feature,threshold,right=True):
        """
        Given a feature and a threshold of it return the TP,TN,FP,FN values.
        """
        data_xled = self.get_values(feature, pdb=None, xled=True)
        data_noxled = self.get_values(feature, pdb=None, xled=False)
        x_xled, counts_xled = get_cumulative(data_xled, bins=[threshold])
        x_noxled, counts_noxled = get_cumulative(data_noxled, bins=[threshold])
        if right:
            TP = counts_xled[0]
            FN = len(data_xled) - counts_xled[0]
            TN = len(data_noxled) - counts_noxled[0]
            FP = counts_noxled[0]
        else:
            FN = counts_xled[0]
            TP = len(data_xled) - counts_xled[0]
            FP = len(data_noxled) - counts_noxled[0]
            TN = counts_noxled[0]
        return TP, TN, FP, FN

    def get_ROC(self, feature, pdb=None, right=True, thresholds=None, whole_db=True, n_iter=100):
        """
        Plot the ROC curve for the given feature and pdb (optional).
        • thresholds: Optional list of thresholds to use instead of taking all
        possible thresholds
        • whole_db: if False, take n (given by n_iter argument) equally
        populated samples for not experimentally observed crosslinks and
        experimentally observed crosslinks.
        """
        if whole_db:
            data_xled = self.get_values(feature, pdb=pdb, xled=True)
            data_noxled = self.get_values(feature, pdb=pdb, xled=False)
            if thresholds is None:
                thresholds = numpy.unique(numpy.r_[data_xled, data_noxled])
            x_xled, counts_xled = get_cumulative(data_xled, bins=thresholds)
            x_noxled, counts_noxled = get_cumulative(data_noxled, bins=thresholds)
            fpr = counts_noxled/len(data_noxled)
            tpr = counts_xled/len(data_xled)
            if not right:
                fpr = 1. - fpr
                tpr = 1. - tpr
                fpr = fpr[::-1]
                tpr = tpr[::-1]
            auc = numpy.trapz(tpr, x=fpr)
            fpr = numpy.r_[0, fpr, 1]
            tpr = numpy.r_[0, tpr, 1]
            return fpr, tpr, auc
        else:
            fpr, tpr, auc = [], [], []
            samples = []
            for i in range(n_iter):
                samples.append(self._get_equal_population())
            thresholds = []
            for db in samples:
                thresholds.extend(list(db.get_values(feature)))
            thresholds = numpy.unique(thresholds)
            for sample in samples:
                fpr_, tpr_, auc_ = sample.get_ROC(feature, thresholds=thresholds)
                if not right:
                    fpr_ = 1. - fpr_
                    tpr_ = 1. - tpr_
                    fpr_ = fpr_[::-1]
                    tpr_ = tpr_[::-1]
                fpr.append(fpr_)
                tpr.append(tpr_)
                auc.append(auc_)
            fpr = numpy.asarray(fpr)
            tpr = numpy.asarray(tpr)
            auc = numpy.asarray(auc)
            fpr = (fpr.mean(axis=0), fpr.std(axis=0))
            tpr = (tpr.mean(axis=0), tpr.std(axis=0))
            auc = (auc.mean(axis=0), auc.std(axis=0))
            if whole_db:
                fpr = numpy.r_[0, fpr, 1]
                tpr = numpy.r_[0, tpr, 1]
            return fpr, tpr, auc

    def plot_ROC(self, feature, pdb=None, right=True, whole_db=True, n_iter=100):
        """
        Plot the ROC curve for the given feature and pdb (optional).
        • whole_db: if False, take n (given by n_iter argument) equally
        populated samples for not experimentally observed crosslinks and
        experimentally observed crosslinks.
        """
        if pdb is not None:
            pdblabel = str(pdb)
        else:
            pdblabel = "Whole DB"
        if type(feature) is str:
            feature = [feature, ]
            right = [right, ]
        for i, feature_ in enumerate(feature):
            fpr, tpr, auc = self.get_ROC(feature_, pdb=pdb, right=right[i], whole_db=whole_db, n_iter=n_iter)
            if not whole_db:
                fpr, err = fpr
                tpr = tpr[0] # Because the std of tpr is always 0
                auc, auc_err = auc
            if whole_db:
                plt.plot(fpr, tpr, label='%s\n(AUC=%.2f)'%(feature_, auc))
            else:
                plt.plot(fpr, tpr, label='%s\n(AUC=%.2f +- %.2f)'%(feature_, auc, auc_err))
                plt.fill_betweenx(tpr, fpr-err, fpr+err, alpha=.5)
        plt.xlabel("False positive rate")
        plt.ylabel("True positive rate")
        plt.legend()
        plt.plot([0, 1], [0, 1], 'k--')
        plt.axis('scaled')
        axes = plt.gca()
        axes.set_xlim([0, 1])
        axes.set_ylim([0, 1])
        if whole_db:
            return auc
        else:
            return auc, auc_err
        return auc, auc_err

    def custom_feature(self, features, weights):
        """
        Create a custom feature as a linear combination of features
        • features: list of feature to combine
        • weights: weight of each feature.
        custom_feature = weights[0]*features[0] + weights[1]*features[1] + ...
        """
        custom_feature = weights[0]*self.get_values(features[0])
        for weight, feature in zip(weights[1:], features[1:]):
           custom_feature += weight*self.get_values(feature)
        XL_list = self.get_all()
        assert len(custom_feature) == len(XL_list),\
                "Inconsistancy between the feature length and the total number of crosslinks"
        for i, xl in enumerate(XL_list):
            xl.custom_feature = custom_feature[i]

    def maximize_auc(self, features, weights):
        """
        Optimize the weights to maximize the AUC for the given features
        """
        def to_min(weights_):
            """
            The function to minimize
            """
            self.custom_feature(features, weights_)
            fpr, tpr, auc = self.get_ROC('custom_feature')
            return 1. - auc
        res = scipy.optimize.minimize(to_min, weights, method='Powell')
        self.custom_feature(features, res.x)
        return res

    def maximize_accuracy(self, features, weights):
        """
        Optimize the weights to maximize the accuracy for the linear
        combination of the given features
        """
        def to_min(weights_):
            """
            The function to minimize
            """
            self.custom_feature(features, weights_)
            data = self.get_values('custom_feature')
            expdata = self.get_values('expcross')
            thresholds = numpy.unique(data)
            acc = _get_accuracy(data, thresholds, expdata, right=True)
            return 1. - acc.max()
        res = scipy.optimize.minimize(to_min, weights, method='Powell')
        self.custom_feature(features, res.x)
        return res


    def plot_joint_distribution(self, feature1, feature2, cumulative=False, pdb=None):
        """
        Plot the joint distribution of feature1 and feature2
        """
        sample = self._get_equal_population(pdb=pdb)
        data1_xled = sample.get_values(feature1, pdb=pdb, xled=True)
        data1_noxled = sample.get_values(feature1, pdb=pdb, xled=False)
        data2_xled = sample.get_values(feature2, pdb=pdb, xled=True)
        data2_noxled = sample.get_values(feature2, pdb=pdb, xled=False)
        xmin = min([min(data1_xled), min(data1_noxled)])
        xmax = numpy.max(data1_xled)
        ymin = min([min(data2_xled), min(data2_noxled)])
        ymax = numpy.max(data2_xled)
        if not cumulative:
            g = sns.kdeplot(data1_xled, data2_xled, cmap="Blues", n_levels=6)
            g = sns.kdeplot(data1_noxled, data2_noxled, cmap="Greens", n_levels=6)
        else:
            bins_xled, counts_xled = get_cumulative(numpy.asarray([data1_xled, data2_xled]))
            bins_noxled, counts_noxled = get_cumulative(numpy.asarray([data1_noxled, data2_noxled]))
            percents = [v/100. for v in range(10, 100, 10)]
            levels = [p*len(data1_xled) for p in percents]
            clabels = {p*len(data1_xled): "%d%%"%(p*100) for p in percents}
            X, Y = numpy.meshgrid(*bins_xled)
            Z = counts_xled
            C1 = plt.contour(X, Y, Z, colors='b', levels=levels, linewidths=1.)
            X, Y = numpy.meshgrid(*bins_noxled)
            Z = counts_noxled
            C2 = plt.contour(X, Y, Z, colors='g', levels=levels, linewidths=1.)
            plt.clabel(C1, inline=1, fontsize=10, fmt=clabels, manual=True)
            plt.clabel(C2, inline=1, fontsize=10, fmt=clabels, manual=True)
        plt.xlabel(feature1)
        plt.ylabel(feature2)
        axes = plt.gca()
        axes.set_xlim([xmin, xmax])
        #return bins_xled, counts_xled, bins_noxled, counts_noxled

    def _get_equal_population(self, pdb=None):
        """
        Get equal number of crosslinked and not crosslinked (experimentally)
        • pdb (optional): A pdb code or a list of pdb codes
        """
        if pdb is not None:
            if type(pdb) is str:
                pdb = [pdb, ]
            XL_list = []
            for pdb_ in pdb:
                XL_list.extend(self.get(pdb_))
        else:
            XL_list = self.get_all()
        XL_list = numpy.asarray(XL_list)
        expcross = numpy.asarray([xl.expcross for xl in XL_list])
        n_xl = expcross.sum()
        p = 1.-expcross
        if p.sum() > 0:
            p/=p.sum()
            not_XLed = numpy.random.choice(XL_list, size=n_xl, p=p)
        else:
            not_XLed = []
        XLed = XL_list[expcross]
        XL_list = list(numpy.r_[XLed, not_XLed])
        xldb = XLDB()
        for xl in XL_list:
            xldb.add(xl.pdb, xl)
        return xldb

    def get_partition(self,n):
        crossexp = self.get_values(feature="expcross")
        xldb_EyXL = self.get_sub_db(crossexp)
        XL_list_EyXL = xldb_EyXL.get_all()
        numpy.random.shuffle(XL_list_EyXL)
        xldb_EnXL = self.get_sub_db(~crossexp)
        XL_list_EnXL = xldb_EnXL.get_all()
        numpy.random.shuffle(XL_list_EnXL)
        set_size_EyXL = len(XL_list_EyXL)/n
        set_size_EnXL = len(XL_list_EnXL)/n
        XL_lists_EyXL = []
        XL_lists_EnXL = []
        for i in range(0,len(XL_list_EyXL),set_size_EyXL):
             XL_lists_EyXL.append(XL_list_EyXL[i:i+set_size_EyXL])
        for i in range(0,len(XL_list_EnXL),set_size_EnXL):
             XL_lists_EnXL.append(XL_list_EnXL[i:i+set_size_EnXL])
        xldb_partition = []
        for i, partition in enumerate(XL_lists_EyXL):
            if i < n:
                xldb = XLDB()
                for xl in partition:
                    xldb.add(xl.pdb, xl, filename=xl.filename, xl_key=xl.key)
                xldb_partition.append(xldb)
        for i, partition in enumerate(XL_lists_EnXL):
            if i < n:
                xldb = xldb_partition[i]
                for xl in partition:
                    xldb.add(xl.pdb,xl, filename=xl.filename, xl_key=xl.key)
        return xldb_partition

    def _get_random_population(self, n, pdb=None):
        """
        Get a random sample of size n from the total population
        """
        if pdb is not None:
            if type(pdb) is str:
                pdb = [pdb, ]
            XL_list = []
            for pdb_ in pdb:
                XL_list.extend(self.get(pdb_))
        else:
            XL_list = self.get_all()
        XL_list = numpy.random.choice(XL_list, size=n)
        xldb = XLDB()
        for xl in XL_list:
            xldb.add(xl.pdb, xl)
        return xldb

    def random_forest(self, features, pdb=None, n_iter=100):
        """
        Compute a random forest classifier using given features as descriptors
        • features: list of features
        """
        samples = []
        for i in range(n_iter):
            sample = self._get_equal_population(pdb=pdb)
            samples.append(sample)

        descriptors = []
        expdata = []
        for sample in samples:
            descriptors_ = []
            expdata.append(list(sample.get_values('expcross')))
            for feature in features:
                descriptors_.append(list(sample.get_values(feature)))
            descriptors.append(numpy.asarray(descriptors_).T)
        descriptors = numpy.asarray(descriptors) # shape: (n_iter, n_xl, len(features))
        expdata = numpy.asarray(expdata) # Target values of shape (n_iter, n_xl)
        scores = []
        feature_importances = []
        progress = Progress(len(descriptors))
        for i, desc in enumerate(descriptors):
            progress.step()
            target = expdata[i]
            X_train, X_test, y_train, y_test = _train_test_split(desc, target)
            clf = RandomForestClassifier(n_estimators=100, max_features=None)
            clf.fit(X_train, y_train)
            y_pred = clf.predict(X_test)
            scores.append((y_test == y_pred).sum()/float(len(y_test)))
            feature_importances.append(clf.feature_importances_)
        scores = numpy.asarray(scores)
        feature_importances = numpy.asarray(feature_importances)
        return scores, feature_importances

    def get_score(self, feature='energy_total', pdb=None, n_iter=100, right=True):
        """
        Compute the prediction score, for the given feature
        """
        samples = []
        for i in range(n_iter):
            sample = self._get_equal_population(pdb=pdb)
            samples.append(sample)

        thresholds = []
        for sample in samples:
            data = list(sample.get_values(feature))
            thresholds.extend(data)
        thresholds = numpy.unique(data)

        scores = []
        random_scores = []
        for sample in samples:
            data = sample.get_values(feature)
            expdata = sample.get_values('expcross')
            random_expdata = numpy.zeros_like(expdata)
            n, p = len(expdata), sum(expdata)
            random_sample = self._get_random_population(n, pdb=pdb)
            random_data = random_sample.get_values(feature)
            if p > 0:
                random_expdata[numpy.random.choice(n, size=p, replace=False)] = True
            scores.append(_get_accuracy(data, thresholds, expdata, right=right))
            random_scores.append(_get_accuracy(random_data, thresholds, random_expdata, right=right))
        scores = numpy.asarray(scores)
        random_scores = numpy.asarray(random_scores)
        scores_mean = scores.mean(axis=0)
        scores_std = scores.std(axis=0)
        random_scores_mean = random_scores.mean(axis=0)
        random_scores_std = random_scores.std(axis=0)
        get_p_value = lambda val, mu, sigma: 1.-scipy.stats.norm(loc=mu, scale=sigma).cdf(val)
        p_values = []
        for i, t in enumerate(thresholds):
            mu = random_scores_mean[i]
            sigma = random_scores_std[i]
            val = scores_mean[i]
            p_values.append(get_p_value(val, mu, sigma))
        p_values = numpy.asarray(p_values)
        self.random_scores = numpy.c_[random_scores_mean, random_scores_std].T
        self.scores = numpy.c_[scores_mean, scores_std, p_values].T
        self.thresholds = thresholds
        ind = self.scores[0].argmax()
        self.score = self.scores[:, ind] # Give the triplet for best score (mu, sigma, p-value)
        self.threshold = self.thresholds[ind]
        return thresholds, scores_mean, scores_std, p_values, random_scores_mean, random_scores_std

    def get_score_per_pdb(self, n_iter=100, feature='energy_total', right=True):
        """
        Compute the classifier score for each pdb of the DB with at least 1 XL
        """
        for pdb in self.pdbs:
            n_xl = sum(self.get_values('expcross', pdb=pdb))
            n_nxl = sum(~self.get_values('expcross', pdb=pdb))
            if n_xl > 0:
                x, y, err, p_values, scores_random, std_random = self.get_score(pdb=pdb, feature=feature, right=right)
                ind = y.argmax()
                _pdb = self.get_pdb(pdb)
                _pdb.score = (y[ind], err[ind], p_values[ind])
                _pdb.threshold = x[ind]
                _pdb.n_xl = n_xl
                _pdb.n_nxl = n_nxl
                fpr, tpr, _pdb.auc = self.get_ROC(feature, pdb=pdb)
                _pdb.print_data()

    def plot_score(self, feature='energy_total', pdb=None, right=True, kappa=False, semilog=False, n_iter=100):
        """
        Plot the classification score (accuracy) as a function of the feature.
        • right: the data are classified as crosslinked if data <= threshold
        • kappa: if True, plot the Cohen's kappa curve
        • semilog: if True, plot a log scale for x axis
        """
        self.get_score(feature=feature, pdb=pdb, right=right, n_iter=n_iter)
        data = (self.scores[0][:, None] + numpy.c_[self.scores[1], -self.scores[1]])
        if pdb is None:
            label = "Whole DB"
        else:
            label = pdb
        if kappa:
            fpr = _get_fpr(self.get_values(feature), self.thresholds, self.get_values('expcross'))
            fpr, indices = numpy.unique(fpr, return_index=True)
            data = data[indices]
            kappa_value = (data - self.random_scores[0][indices, None]) / (1. - self.random_scores[0][indices, None])
            data = kappa_value
            x_values = fpr
            auk = numpy.trapz(data[:, 0], x=fpr)
        else:
            x_values = self.thresholds
        if semilog:
            plt.xscale('symlog')
        sns.tsplot(data=data.T, time=x_values)
        if not kappa:
            plt.xlabel(feature)
            plt.ylabel('Accuracy')
            plt.title(label)
        else:
            plt.xlabel('False Positive Rate')
            plt.ylabel("Cohen's Kappa")
            plt.title("%s (AUK=%.2f)"%(label, auk))
        # Confidence intervals:
        styles = ['k-', 'k--', 'k-.', 'k:']
        for i in range(4):
            random_data = (self.random_scores[0][:, None] + numpy.c_[i*self.random_scores[1], -i*self.random_scores[1]])
            if kappa:
                random_data = random_data[indices]
                random_data = random_data - self.random_scores[0][indices, None]
            plt.plot(x_values, random_data[:,0], styles[i], linewidth=.5)
            plt.plot(x_values, random_data[:,1], styles[i], linewidth=.5)
        if kappa:
            plt.axis('scaled')
            axes = plt.gca()
            axes.set_xlim([0, 1])
            axes.set_ylim([0, 1])

if __name__ == '__main__':
    import XLDB
    xl_list = sys.argv[1:]
    xldb = XLDB.XLDB()
    sys.stdout.write("Loading XLs...\n")
    progress = Progress(len(xl_list))
    for filename in xl_list:
        progress.step()
        pdbname = os.path.dirname(filename)
        xls = pickle.load(open(filename))
        for k in xls:
            xl = xls[k]
            xldb.add(pdbname, xl, filename=filename, xl_key=k)
    pickle.dump(xldb, open('xldb.dat', 'w'), 1)
